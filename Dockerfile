

############################################################
# Dockerfile to build Pipeline Factory container images
# Based on Ubuntu
############################################################

# Set the base image to Ubuntu
FROM ubuntu

# File Author / Maintainer
MAINTAINER Radhouane Aniba

# Update the repository sources list
RUN apt-get update

# Install wget

RUN apt-get install wget --assume-yes

# Install Git

RUN apt-get install git-core --assume-yes

# Install Python

RUN apt-get install build-essential --assume-yes
RUN apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev --assume-yes
RUN mkdir ~/software-downloads
RUN (cd ~/software-downloads && wget --no-check-certificate  http://python.org/ftp/python/2.7.5/Python-2.7.5.tgz)
RUN (cd ~/software-downloads && tar -xvf Python-2.7.5.tgz && cd Python-2.7.5 && ./configure && make && sudo make install)

# Install pip

RUN apt-get install python-pip python-dev build-essential python-yaml --assume-yes


# OR Install and use virtual environment which is much better
# But remember to swich to the environment built after tty session

RUN pip install virtualenv
RUN virtualenv ~/my_env
RUN /bin/bash -c 'source ~/my_env/bin/activate'

# Install YAML

RUN (cd ~/software-downloads && wget --no-check-certificate http://pyyaml.org/download/pyyaml/PyYAML-3.11.tar.gz)
RUN (cd ~/software-downloads && tar -xvf PyYAML-3.11.tar.gz && cd PyYAML-3.11 && python setup.py --without-libyaml install)

# Install pipeline factory

RUN env GIT_SSL_NO_VERIFY=true git clone https://shah_lab_proxy:week90nedela@svn.bcgsc.ca/stash/scm/pf/pipeline-factory.git
RUN (cd pipeline-factory && sudo python setup.py install)


# Set default container command
ENTRYPOINT /bin/bash

