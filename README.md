![logo](https://bytebucket.org/radaniba/pipeline-factory-docker/raw/1cd385ca5315d74a93bbaf11940841593c20e1be/docker-factory-logo.png)


# Pipeline Factory Docker Image

This is a Tutorial on how to build a docker image for the pipeline factory and get started using the pipeline with no hassles for the configuration. 

The tutorial will guide you through the path to get the factory up and running in your machine, running the pipeline with a real example (pipeline) is not discussed here

## Step 1 : Prepare the field for Docker : using boot2docker

It is very easy to get docker installed on your local machine, for the purpose of this tutorial we are not going to talk about running pipelines on the cluster but rather locally so when you test the pipeline please make sure to run small tests and not heavy pipelines

Docker team made a small neat utility that will install a virtual machine on your local machine (I am assuming you are using MacOS, but it is totally doable on windows as well)

In this tutorial we are going to install pipeline factory on an ubuntu base image

To install boot2docker please go to [http://boot2docker.io](http://boot2docker.io/) the instruction is straitforward, basically it is a binary file that you download and install on your Mac

Locate the Boot2Docker app in your Applications folder and run it. Or, you can initialize Boot2Docker from the command line by running:

```
boot2docker init
boot2docker start
boot2docker ssh
```

If everything is Ok you should see this 

```
                        ##        .
                  ## ## ##       ==
               ## ## ## ##      ===
           /""""""""""""""""\___/ ===
      ~~~ {~~ ~~~~ ~~~ ~~~~ ~~ ~ /  ===- ~~~
           \______ o          __/
             \    \        __/
              \____\______/
 _                 _   ____     _            _
| |__   ___   ___ | |_|___ \ __| | ___   ___| | _____ _ __
| '_ \ / _ \ / _ \| __| __) / _` |/ _ \ / __| |/ / _ \ '__|
| |_) | (_) | (_) | |_ / __/ (_| | (_) | (__|   <  __/ |
|_.__/ \___/ \___/ \__|_____\__,_|\___/ \___|_|\_\___|_|
Boot2Docker version 1.3.2, build master : 495c19a - Mon Nov 24 20:40:58 UTC 2014
Docker version 1.3.2, build 39fa2fa
docker@boot2docker:~$
```

At this stage you will enter the VM and will start building the image for the factory

## Step 2 : Build the Dockerfile and build the factory image

To build a Docker image, we can do all the steps one by one, which I am going to explain here, but Docker comes with a smarter way of doing things, especially if we are going to deal with a lot of dependencies and utilities. There is file called `Dockerfile` that we need to create, you can think of it like a shell script containing all the steps of creating our docker image.

so to get started lets create that empty Dockerfile for now, and name it litterally `Dockerfile`

```
touch Dockerfile
```

Now we are going to copy this content into that Dockerfile

```
############################################################
# Dockerfile to build Pipeline Factory container images
# Based on Ubuntu
############################################################

# Set the base image to Ubuntu
FROM ubuntu

# File Author / Maintainer
MAINTAINER Radhouane Aniba

# Update the repository sources list
RUN apt-get update

# Install wget

RUN apt-get install wget --assume-yes

# Install Git

RUN apt-get install git-core --assume-yes

# Install Python

RUN apt-get install build-essential --assume-yes
RUN apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev --assume-yes
RUN mkdir ~/software-downloads
RUN (cd ~/software-downloads && wget --no-check-certificate  http://python.org/ftp/python/2.7.5/Python-2.7.5.tgz)
RUN (cd ~/software-downloads && tar -xvf Python-2.7.5.tgz && cd Python-2.7.5 && ./configure && make && sudo make install)

# Install pip

RUN apt-get install python-pip python-dev build-essential python-yaml --assume-yes


# OR Install and use virtual environment which is much better
# But remember to swich to the environment built after tty session

RUN pip install virtualenv
RUN virtualenv ~/my_env
RUN /bin/bash -c 'source ~/my_env/bin/activate'

# Install YAML

RUN (cd ~/software-downloads && wget --no-check-certificate http://pyyaml.org/download/pyyaml/PyYAML-3.11.tar.gz)
RUN (cd ~/software-downloads && tar -xvf PyYAML-3.11.tar.gz && cd PyYAML-3.11 && python setup.py --without-libyaml install)

# Install pipeline factory

RUN env GIT_SSL_NO_VERIFY=true git clone https://shah_lab_proxy:week90nedela@svn.bcgsc.ca/stash/scm/pf/pipeline-factory.git
RUN (cd pipeline-factory && sudo python setup.py install)


# Set default container command
ENTRYPOINT /bin/bash

```

The Dockerfile need to have some basic information especially those :

```
FROM ubuntu
Maintainer John Doe
```

This is basically saying, I want to build my image using a very basic ubuntu distribution, and the maintainer of this image is John Doe (you can put your email if you want to push the image to Docker index)

The distribution I tested while writing this tutorial is 

```
Distributor ID: Ubuntu
Description:    Ubuntu 14.04.1 LTS
Release:    14.04
Codename:   trusty
```

You will notice that the file contains the command `RUN` before each command, this is the way Docker proceeds to get instructions from the Dockerfile

```
RUN this and that
```

will be interpreted as a command called `this and that`

All we have to do now is to add all things we need to get started with all dependencies

After adding all the ingredient we save the file and we exit

To build the image we run this command 

```
sudo docker build -t pipeline_factory .
```

This will create a docker image called `pipeline_factory`

After running this command you should see something more elaborate than this, but as I did that already on my machine, all the steps are skept but I am adding the stages here so that you see what Docker is doing

```
Sending build context to Docker daemon 20.48 kB
Sending build context to Docker daemon
Step 0 : FROM ubuntu
 ---> 9bd07e480c5b
Step 1 : MAINTAINER Radhouane Aniba
 ---> Using cache
 ---> 41acd65508ce
Step 2 : RUN apt-get update
 ---> Using cache
 ---> 66bb01278dc9
Step 3 : RUN apt-get install wget --assume-yes
 ---> Using cache
 ---> 177b2a8f9969
Step 4 : RUN apt-get install git-core --assume-yes
 ---> Using cache
 ---> c3b74ea2533e
Step 5 : RUN apt-get install build-essential --assume-yes
 ---> Using cache
 ---> 8ab3c069cb27
Step 6 : RUN apt-get install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev --assume-yes
 ---> Using cache
 ---> 84e94c89dad2
Step 7 : RUN mkdir ~/software-downloads
 ---> Using cache
 ---> 242b12c0f294
Step 8 : RUN (cd ~/software-downloads && wget --no-check-certificate  http://python.org/ftp/python/2.7.5/Python-2.7.5.tgz)
 ---> Using cache
 ---> 38bc9136fbc9
Step 9 : RUN (cd ~/software-downloads && tar -xvf Python-2.7.5.tgz && cd Python-2.7.5 && ./configure && make && sudo make install)
 ---> Using cache
 ---> 14f30dc2ef6d
Step 10 : RUN apt-get install python-pip python-dev build-essential python-yaml --assume-yes
 ---> Using cache
 ---> 771cb609b602
Step 11 : RUN pip install virtualenv
 ---> Using cache
 ---> bd7d8cfd38f9
Step 12 : RUN virtualenv ~/my_env
 ---> Using cache
 ---> 5418ba8aebe7
Step 13 : RUN /bin/bash -c 'source ~/my_env/bin/activate'
 ---> Using cache
 ---> ca5595dcf70c
Step 14 : RUN (cd ~/software-downloads && wget --no-check-certificate http://pyyaml.org/download/pyyaml/PyYAML-3.11.tar.gz)
 ---> Using cache
 ---> e09026c30ca3
Step 15 : RUN (cd ~/software-downloads && tar -xvf PyYAML-3.11.tar.gz && cd PyYAML-3.11 && python setup.py --without-libyaml install)
 ---> Using cache
 ---> 6afe0d3fc2eb
Step 16 : RUN env GIT_SSL_NO_VERIFY=true git clone https://shah_lab_proxy:week90nedela@svn.bcgsc.ca/stash/scm/pf/pipeline-factory.git
 ---> Using cache
 ---> 457e619538ee
Step 17 : RUN (cd pipeline-factory && sudo python setup.py install)
 ---> Using cache
 ---> b9fa57ea690f
Step 18 : ENTRYPOINT /bin/bash
 ---> Using cache
 ---> 33aedc00ff53
Successfully built 33aedc00ff53
```

## Step 3 : Start using the pipeline factory

Congratulations ! you created your first pipeline_factory docker image. Now let's use it

Open your terminal and type :

```
docker run -i -t pipeline_factory /bin/bash
```

which means docker will open the pipeline_factory image in an interactive way and it will let you enter the factory using the tty 

Notice here that your terminal invte will change, that's normal.

At this stage, you're in another universe, your entire system will be :

```
.
|-- bin
|-- boot
|-- dev
|-- etc
|-- home
|-- lib
|-- lib64
|-- media
|-- mnt
|-- opt
|-- pipeline-factory
|-- proc
|-- root
|-- run
|-- sbin
|-- srv
|-- sys
|-- tmp
|-- usr
`-- var
```

Notice here the directory : `pipeline-factory`

This means we successfully installed the factory repo from our Stash repo
To make sure let's go in there and test the pipeline

```
root@61d1267dffed:/# cd pipeline-factory/pipeline_factory/
root@61d1267dffed:/pipeline-factory/pipeline_factory# python factory.py -h
usage: factory.py [-h] [-w WORKING_DIR] {make_config,init_pipeline,test} ...

Pipeline Factory

positional arguments:
  {make_config,init_pipeline,test}
                        subparsers for make-config or init-pipeline
    make_config         make a config file
    init_pipeline       initialize a pipeline from the given config file
    test                test the pipeline factory installation by running
                        tests included in the package

optional arguments:
  -h, --help            show this help message and exit
  -w WORKING_DIR, --working_dir WORKING_DIR
                        path of the working dir
```

and that's definitely a confirmation that the factory is installed.

Now all you have to do is to create a config file and start using the pipeline

## Step 4 : Exit to see what you did

Ok, now let type `exit` to get out of this image

You should be back to your Macos terminal

To see what we did just type 

```
docker ps -a

docker@boot2docker:~$ docker ps -a
CONTAINER ID        IMAGE                     COMMAND                CREATED             STATUS                        PORTS               NAMES
61d1267dffed        pipeline_factory:latest   "/bin/sh -c /bin/bas   13 minutes ago      Exited (0) 4 seconds ago                          backstabbing_hopper
74e1f4f3f9f2        pipeline_factory:latest   "/bin/sh -c /bin/bas   41 minutes ago      Exited (0) 17 minutes ago                         stoic_euclid
80f250c278f0        5418ba8aebe7              "/bin/sh -c 'source    46 minutes ago      Exited (127) 46 minutes ago                       naughty_nobel

```

This command will give us the active sessions aka `containers` of docker images, if you want to get back into one container you simply do that :

```
docker@boot2docker:~$ docker start 61d1267dffed
61d1267dffed
docker@boot2docker:~$ docker attach 61d1267dffed
root@61d1267dffed:/#
```

And you're back in, now play with the arrows or type `history` to see what you did before, and you'll notice that everything is there


## Step 5 : You're done

If you followed this tutorial you now know how to 

- Install docker
- Create a Dockerfile
- Create a docker image
- Run docker images